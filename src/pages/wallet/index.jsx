import React from 'react';
import WalletBalance from 'widgets/WalletBalance';
import Transactions from 'widgets/Transactions';
import WalletActions from 'widgets/WalletActions';

const Wallet = () => (
  <div>
    <WalletBalance />
    <WalletActions />
    <Transactions />
  </div>
);

export default Wallet;
