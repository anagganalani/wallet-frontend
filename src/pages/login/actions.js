import { createAction } from 'redux-actions';
import { fetchApi, tokenName } from 'utils';
import { LOGIN, CLIENT_ID, CLIENT_SECRET } from './constants';

const LOGIN_PATH = process.env.REACT_APP_LOGIN_PATH;

const reset = createAction(LOGIN, () => ({
  status: 'initial'
}));

const begin = createAction(LOGIN, () => ({
  status: 'pending'
}));

const success = createAction(LOGIN, token => {
  window.localStorage[tokenName] = token;
  return {
    token,
    status: 'success'
  };
});

const fail = createAction(LOGIN, error => ({
  error,
  status: 'error'
}));

const login = (username, password, code) => dispatch => {
  dispatch(begin());
  const body = {
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    pin: code,
    grant_type: 'password',
    scope: 'panel',
    username,
    password
  };
  fetchApi({
    body,
    path: LOGIN_PATH,
    method: 'post'
  })
    .then(response => {
      const user = response;
      dispatch(success(user.access_token));
    })
    .catch(error => {
      dispatch(fail(error));
    });
};

const actions = {
  reset,
  begin,
  success,
  fail,
  login
};

export default actions;
