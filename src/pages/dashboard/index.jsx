import React from 'react';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import dashboardStyle from 'assets/jss/material-dashboard-react/views/dashboardStyle';
import CurrenciesValue from 'widgets/CurrenciesValue';
import Earnings from 'widgets/Earnings';
import MonthEarnings from 'widgets/MonthEarnings';
import LastTransactions from 'widgets/LastTransactions';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';

const Dashboard = () => (
  <div>
    <CurrenciesValue />
    <MonthEarnings />
    <GridContainer
      direction="row"
      justify="space-between"
      alignItems="flex-start"
      container
    >
      <GridItem xs={7}>
        <LastTransactions />
      </GridItem>
      <GridItem xs={5}>
        <Earnings />
      </GridItem>
    </GridContainer>
  </div>
);

export default withStyles(dashboardStyle)(Dashboard);
