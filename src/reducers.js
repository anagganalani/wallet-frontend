import { combineReducers } from 'redux';
import { currencies_value } from 'widgets/CurrenciesValue/reducers';
import { last_transactions } from 'widgets/LastTransactions/reducers';
import { earnings } from 'widgets/Earnings/reducers';
import { month_earnings } from 'widgets/MonthEarnings/reducers';
import { transactions } from 'widgets/Transactions/reducers';
import { wallet_actions } from 'widgets/WalletActions/reducers';
import { user, wallet } from 'layouts/reducers';
import { login } from 'pages/login/reducers';

const reducers = {
  currencies_value,
  earnings,
  last_transactions,
  login,
  month_earnings,
  user,
  wallet,
  wallet_actions,
  transactions
};

export default combineReducers(reducers);
