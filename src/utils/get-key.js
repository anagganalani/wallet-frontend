/*
  Code by Moshe Kolodny
  https://github.com/kolodny/weak-key
*/

const map = new WeakMap();
let index = 0;

/**
 *
 * Generates a simple key for an object, by storing the object in a
 * WeakMap. This key can be used in
 * React component lists as the property key={}
 *
 * @param {Object} obj
 * @returns {string}
 */
function getKey(obj) {
  let key = map.get(obj);
  if (!key) {
    key = `key-${index++}`;
    map.set(obj, key);
  }
  return key;
}

export { getKey };
