import axios from 'axios';

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;
export const tokenName = 'auth';

export function getAuthToken() {
  if (!window.localStorage[tokenName]) {
    return undefined;
  }
  return window.localStorage[tokenName];
}

export function logOut() {
  window.localStorage.removeItem(tokenName);
}

export function isLoggedIn() {
  return Boolean(getAuthToken());
}

export function requireAuth(nextState, replace) {
  if (!isLoggedIn()) {
    replace('/');
  }
}

export function fetchApi(args) {
  const { path, body, method } = args;
  const url = `${BACKEND_URL}/${path}`;
  const newargs = {
    url,
    data: body,
    method: method || 'GET',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    }
  };
  if (!isLoggedIn()) {
    delete newargs.headers.Authorization;
  }
  return axios(newargs)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      if (error.response.status === 401 && isLoggedIn()) {
        logOut();
        window.history.go('/login');
      }
    });
}

export * from './get-key';
