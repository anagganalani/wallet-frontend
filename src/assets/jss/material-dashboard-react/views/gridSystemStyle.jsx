import { grayColor } from 'assets/jss/material-dashboard-react';

const gridSystemStyle = {
  title: {
    color: grayColor[2],
    textDecoration: 'none'
  }
};

export default gridSystemStyle;
