import { createAction } from 'redux-actions';
import { fetchApi, logOut } from 'utils';
import { USER, WALLET } from './constants';

const USER_WALLET_PATH = process.env.REACT_APP_USER_WALLET_PATH;
const USER_ACCOUNT_PATH = process.env.REACT_APP_USER_ACCOUNT_PATH;

const reset = createAction(USER, () => ({
  status: 'initial'
}));

const begin = createAction(USER, () => ({
  status: 'pending'
}));

const fail = createAction(USER, error => ({
  error,
  status: 'error'
}));

const success = createAction(USER, user => ({
  user,
  status: 'success'
}));

const resetWallet = createAction(WALLET, () => ({
  status: 'initial'
}));

const beginWallet = createAction(WALLET, () => ({
  status: 'pending'
}));

const failWallet = createAction(WALLET, error => ({
  error,
  status: 'error'
}));

const successWallet = createAction(WALLET, wallet => ({
  wallet,
  status: 'success'
}));

const getMe = () => dispatch => {
  dispatch(begin());
  dispatch(beginWallet());
  Promise.all([
    fetchApi({ path: USER_ACCOUNT_PATH }),
    fetchApi({ path: USER_WALLET_PATH })
  ])
    .then(responses => {
      const user = responses[0];
      const wallet = responses[1];
      dispatch(success(user.data));
      dispatch(successWallet(wallet.data));
    })
    .catch(error => {
      logOut();
      dispatch(fail(error));
      dispatch(failWallet(error));
    });
};

const actions = {
  reset,
  begin,
  success,
  fail,
  resetWallet,
  beginWallet,
  successWallet,
  failWallet,
  getMe
};

export default actions;
