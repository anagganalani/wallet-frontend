import React from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { isLoggedIn } from 'utils';

import { Switch, Route, Redirect } from 'react-router-dom';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

// core components
import Header from 'components/Header/Header';
import Sidebar from 'components/Sidebar/Sidebar';

import dashboardRoutes from 'routes/dashboard';

import appStyle from 'assets/jss/material-dashboard-react/layouts/dashboardStyle';

import image from 'assets/img/sidebar-2.jpg';
import logo from 'logo.jpg';
import actions from './actions';

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map(prop => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.pathTo} key={prop.id} />;
      if (prop.collapse)
        return prop.views.map(prop => {
          return (
            <Route path={prop.path} component={prop.component} key={prop.id} />
          );
        });
      return (
        <Route path={prop.path} component={prop.component} key={prop.id} />
      );
    })}
  </Switch>
);

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileOpen: false,
      miniActive: false,
      loading: true
    };

    this.mainPanelRef = React.createRef();

    this.getRoute = this.getRoute.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.sidebarMinimize = this.sidebarMinimize.bind(this);
    this.resizeFunction = this.resizeFunction.bind(this);
  }

  // TODO: fix me
  UNSAFE_componentWillMount() {
    if (!isLoggedIn()) {
      this.props.history.push('/login');
    }
    this.props.getMe();
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'success') {
      this.setState({ loading: false });
    } else if (nextProps.status === 'error') {
      this.props.history.push('/login');
    }
  }

  getRoute = () => {
    return this.props.location.pathname !== '/maps/full-screen-maps';
  };

  handleDrawerToggle = () => {
    this.setState(prevState => ({ mobileOpen: !prevState.mobileOpen }));
  };

  sidebarMinimize = () => {
    this.setState(prevState => ({ miniActive: !prevState.miniActive }));
  };

  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  };

  render() {
    const { classes, user, ...rest } = this.props;
    const { loading } = this.state;
    if (loading) {
      return 'Loading';
    }
    const mainPanel = `${classes.mainPanel} ${cx({
      [classes.mainPanelSidebarMini]: this.state.miniActive,
      [classes.mainPanelWithPerfectScrollbar]:
        navigator.platform.indexOf('Win') > -1
    })}`;
    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={dashboardRoutes}
          logoText="BotC Wallet"
          logo={logo}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="green"
          bgColor="black"
          miniActive={this.state.miniActive}
          user={user}
          {...rest}
        />
        <div className={mainPanel} ref={this.mainPanelRef}>
          <Header
            sidebarMinimize={this.sidebarMinimize}
            miniActive={this.state.miniActive}
            routes={dashboardRoutes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.data,
  status: state.user.status,
  wallet_status: state.wallet.status
});

export default connect(
  mapStateToProps,
  actions
)(withStyles(appStyle)(Dashboard));
