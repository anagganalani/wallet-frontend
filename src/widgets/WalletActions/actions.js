import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { WALLET_ACTIONS } from './constants';

const WALLET_ACTIONS_PATH = process.env.REACT_APP_WALLET_ACTIONS_PATH;

const reset = createAction(WALLET_ACTIONS, () => ({
  status: 'initial'
}));

const begin = createAction(WALLET_ACTIONS, () => ({
  status: 'pending'
}));

const fail = createAction(WALLET_ACTIONS, error => ({
  error,
  status: 'error'
}));

const success = createAction(WALLET_ACTIONS, methods => ({
  methods,
  status: 'success'
}));

const getActions = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: WALLET_ACTIONS_PATH
  })
    .then(response => {
      const methods = response;
      dispatch(success(methods.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getActions
};

export default actions;
