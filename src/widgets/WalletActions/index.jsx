import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './actions';
import CashIn from './CashIn';

class WalletActions extends Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  // TODO: fix me
  UNSAFE_componentWillMount() {
    this.props.getActions();
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'success') {
      this.setState({ loading: false });
    }
  }

  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    return (
      <CashIn
        disabled={this.state.loading && this.props.cash_in.length === 0}
      />
    );
  }
}
const mapStateToProps = state => ({
  status: state.wallet_actions.status,
  cash_in: state.wallet_actions.cash_in,
  cash_out: state.wallet_actions.cash_out
});
export default connect(mapStateToProps, actions)(WalletActions);
