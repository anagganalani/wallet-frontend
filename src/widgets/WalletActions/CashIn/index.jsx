import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';

import Wizard from 'components/Wizard/Wizard';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Slide from '@material-ui/core/Slide';
import SelectMethod from './SelectMethod';
import Cash from './Cash';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class CashIn extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.handleOpen = () => {
      this.setState({ open: true });
    };
  }

  render() {
    const { disabled } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen} disabled={disabled}>
          Cash In
        </Button>
        <Dialog
          fullScreen
          onClose={this.handleClose}
          aria-labelledby="simple-dialog-title"
          open={this.state.open}
          TransitionComponent={Transition}
        >
          <div>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
            <Wizard
              steps={[
                {
                  stepName: 'Choice',
                  stepComponent: SelectMethod,
                  stepId: 'choice'
                },
                {
                  stepName: 'Cash',
                  stepComponent: Cash,
                  stepId: 'cash'
                }
              ]}
              validate
              title="Cash In"
              subtitle=""
              finishButtonClick={this.handleClose}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}
export default CashIn;
