import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import { Card } from '@material-ui/core';
import CashInMethod from './methods';

const style = {
  infoText: {
    fontWeight: '300',
    margin: '10px 0 30px',
    textAlign: 'center'
  },
  inputAdornmentIcon: {
    color: '#555'
  },
  inputAdornment: {
    position: 'relative'
  }
};

/* eslint class-methods-use-this: ["error", { "exceptMethods": ["isValidated"] }] */

class Cash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  sendState() {
    return this.state;
  }

  isValidated() {
    return true;
  }

  render() {
    if (!this.props.allStates.choice) {
      return <div />;
    }

    let Component;

    if (this.props.allStates.choice.selectedMethod) {
      Component = this.props.allStates.choice.selectedMethod;
    }
    return (
      <Card>
        <div id="cash_in_method">
          <CashInMethod method={Component} />
        </div>
      </Card>
    );
  }
}
export default withStyles(style)(Cash);
