import axios from 'axios';
import { createAction } from 'redux-actions';
import { getAuthToken } from 'utils';
import {
  CASH_IN_TOKENS,
  CASH_IN_TOKEN_CREATE,
  CASH_IN_TOKEN_REMOVE
} from './constants';

const apiBaseURL = `${process.env.REACT_APP_BACKEND_URL}/${process.env.REACT_APP_WALLET_PATH}`;

const reset = createAction(CASH_IN_TOKENS, method => ({
  method,
  status: 'initial'
}));

const begin = createAction(CASH_IN_TOKENS, method => ({
  method,
  status: 'pending'
}));

const fail = createAction(CASH_IN_TOKENS, (method, error) => ({
  method,
  error,
  status: 'error'
}));

const success = createAction(CASH_IN_TOKENS, (method, list) => ({
  method,
  list,
  status: 'success'
}));

const create = createAction(CASH_IN_TOKEN_CREATE, (method, token) => ({
  method,
  token,
  status: 'created'
}));

const remove = createAction(CASH_IN_TOKEN_REMOVE, (method, id) => ({
  method,
  id,
  status: 'removed'
}));

const createCashInToken = (method, label) => dispatch => {
  const body = new FormData();
  body.set('method', `${method.cname}-in`);
  body.set('label', label);
  axios({
    method: 'POST',
    data: body,
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    },
    url: `${apiBaseURL}/cash_in_token`
  })
    .then(response => {
      const cash_in_token = response.data;
      dispatch(create(method, cash_in_token.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const removeCashInToken = (method, id) => dispatch => {
  const body = new FormData();
  body.set('disable', true);
  axios({
    method: 'PUT',
    data: 'disable=true',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    url: `${apiBaseURL}/cash_in_token/${id}?`
  })
    // TODO: fix me
    .then(() => {
      dispatch(remove(method, id));
      // getCashInList(method);
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const getCashInList = method => dispatch => {
  dispatch(begin(method));
  axios({
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    },
    url: `${apiBaseURL}/cash_in_token/${method.cname}-in`
  })
    .then(response => {
      const cash_in_list = response.data;
      dispatch(success(method, cash_in_list.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getCashInList,
  createCashInToken,
  removeCashInToken
};

export default actions;
