import React, { Component } from 'react';
import { Paper, Card } from '@material-ui/core';
import Table from 'components/Table/Table';
import Close from '@material-ui/icons/Close';
import Button from 'components/CustomButtons/Button';
import SweetAlert from 'react-bootstrap-sweetalert';
import QRCode from 'qrcode.react';
import TextField from '@material-ui/core/TextField';

class Coin extends Component {
  constructor() {
    super();
    this.state = {
      showQr: null,
      label: ''
    };
  }

  hideQr() {
    this.setState({ showQr: null });
  }

  showQr(element) {
    const { token, label } = element;
    const text = `bitcoin:${token.replace(
      /[^a-zA-Z 0-9.]+/g,
      ' '
    )}?amount=0&label=${label.replace(/[^a-zA-Z 0-9.]+/g, ' ')}`;
    this.setState({
      showQr: (
        <SweetAlert
          style={{ display: 'block', marginTop: '-200px' }}
          title={label || 'Unlabeled'}
          onConfirm={() => this.hideQr()}
          onCancel={() => this.hideQr()}
        >
          <QRCode level="H" size={200} value={text} />
        </SweetAlert>
      )
    });
  }

  removeToken(id) {
    this.props.removeCashInToken(this.props.method, id);
  }

  createCashInToken() {
    this.props.createCashInToken(this.props.method, this.state.label);
    this.setState({ label: '' });
  }

  render() {
    const { list, status } = this.props;
    let elements = [];
    if (list.elements) {
      elements = list.elements.map(element => [
        element.id,
        element.label,
        element.token,
        <div>
          <Button onClick={() => this.showQr(element)}> Show Qr</Button>
          <Button
            onClick={() => this.removeToken(element.id)}
            color="danger"
            key={element.id}
          >
            <Close />
          </Button>
        </div>
      ]);
    }
    let createToken = '';
    if (elements.length < 5 && status !== 'pending') {
      createToken = (
        <div>
          <TextField
            label="label"
            value={this.state.label}
            onChange={event => this.setState({ label: event.target.value })}
          />
          <Button onClick={() => this.createCashInToken()} color="success">
            {' '}
            Generate new {this.props.method.cname} address{' '}
          </Button>
        </div>
      );
    } else if (elements.length === 5)
      createToken =
        'if you want to generate a new address, you have to delete a old address';
    return (
      <Paper>
        <Card>{createToken}</Card>
        {this.state.showQr}
        <Table
          tableHead={['id', 'Label', 'Token', 'Actions']}
          tableData={elements}
        />
        {elements.length === 0 && status !== 'pending' ? 'No data found' : ''}
      </Paper>
    );
  }
}

export default Coin;
