import { WALLET_ACTIONS } from './constants';
import {
  CASH_IN_TOKENS,
  CASH_IN_TOKEN_CREATE,
  CASH_IN_TOKEN_REMOVE
} from './CashIn/methods/constants';

// TODO: handle errors
export function wallet_actions(
  state = { status: 'initial', cash_in: [], cash_out: [] },
  action
) {
  switch (action.type) {
    case WALLET_ACTIONS: {
      const { error, status } = action.payload;
      let { methods } = action.payload;
      let { cash_in, cash_out } = state;
      if (methods) {
        let sw = false;
        let sw2 = false;
        methods = methods.filter(method => {
          let result = false;
          if (method.cname === 'halcash_es') {
            sw = true;
          }
          if (method.cname === 'sepa' && method.type === 'out') {
            sw2 = true;
          }
          if (
            ((method.cname === 'halcash_pl' && sw === false) ||
              method.cname !== 'halcash_pl') &&
            ((method.cname === 'transfer' && sw2 === false) ||
              method.cname !== 'transfer') &&
            method.cname !== 'echo' &&
            method.cname !== 'teleingreso_pt' &&
            method.cname !== 'teleingreso_usa' &&
            method.cname !== 'paynet_reference'
          ) {
            result = true;
          }
          return result;
        });
        cash_out = methods
          .filter(method => method.type === 'out')
          .map(method => ({ ...method, tokens: { status: '', list: [] } }));
        cash_in = methods
          .filter(method => method.type === 'in')
          .map(method => ({ ...method, tokens: { status: '', list: [] } }));
      }
      return {
        cash_in,
        cash_out,
        error,
        status
      };
    }

    case CASH_IN_TOKENS: {
      const { method, status, error, list } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      let tokens = {};
      if (list) {
        tokens = {
          status,
          error,
          list
        };
      } else {
        tokens = {
          status,
          list: cash_in_method.tokens.list
        };
      }
      cash_in_method.tokens = tokens;
      cash_in[indexOfMethod] = cash_in_method;

      return {
        ...state,
        cash_in
      };
    }
    case CASH_IN_TOKEN_CREATE: {
      // eslint-disable-next-line no-unused-vars
      const { method, status, token, error } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      const elements = cash_in_method.tokens.list.elements;
      if (token) {
        elements.push(token);
      }
      cash_in_method.tokens.list.elements = elements;
      cash_in_method.tokens.status = status;
      cash_in[indexOfMethod] = cash_in_method;
      return {
        ...state,
        cash_in
      };
    }
    case CASH_IN_TOKEN_REMOVE: {
      // eslint-disable-next-line no-unused-vars
      const { method, status, id, error } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      let elements = cash_in_method.tokens.list.elements;
      if (id) {
        elements = elements.filter(element => element.id !== id);
      }
      cash_in_method.tokens.list.elements = elements;
      cash_in_method.tokens.status = status;
      cash_in[indexOfMethod] = cash_in_method;
      return {
        ...state,
        cash_in
      };
    }
    default:
      return state;
  }
}
