import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { EARNINGS } from './constants';

const EARNINGS_PATH = process.env.REACT_APP_EARNINGS_PATH;

const reset = createAction(EARNINGS, () => ({
  status: 'initial'
}));

const begin = createAction(EARNINGS, () => ({
  status: 'pending'
}));

const fail = createAction(EARNINGS, error => ({
  error,
  status: 'error'
}));

const success = createAction(EARNINGS, earnings => ({
  earnings,
  status: 'success'
}));

const getEarnings = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: EARNINGS_PATH
  })
    .then(response => {
      const earnings = response;
      dispatch(success(earnings.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getEarnings
};

export default actions;
