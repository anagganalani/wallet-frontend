import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { TRANSACTIONS } from './constants';

const TRANSACTIONS_PATH = process.env.REACT_APP_TRANSACTIONS_PATH;

const reset = createAction(TRANSACTIONS, () => ({
  status: 'initial'
}));

const begin = createAction(TRANSACTIONS, () => ({
  status: 'pending'
}));

const fail = createAction(TRANSACTIONS, error => ({
  error,
  status: 'error'
}));

const success = createAction(TRANSACTIONS, transactions => ({
  transactions,
  status: 'success'
}));

const getTransactions = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: TRANSACTIONS_PATH
  })
    .then(response => {
      const transactions = response.data;
      dispatch(success(transactions.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getTransactions
};

export default actions;
