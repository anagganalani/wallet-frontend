import { TRANSACTIONS } from './constants';

export function transactions(state = { status: 'initial' }, action) {
  switch (action.type) {
    case TRANSACTIONS: {
      const {
        transactions = state.transactions,
        error = state.error,
        status = state.status
      } = action.payload;
      return {
        transactions,
        error,
        status
      };
    }
    default:
      return state;
  }
}
