import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { LAST_TRANSACTIONS } from './constants';

const LAST_TRANSACTIONS_PATH = process.env.REACT_APP_LAST_TRANSACTIONS_PATH;

const reset = createAction(LAST_TRANSACTIONS, () => ({
  status: 'initial'
}));

const begin = createAction(LAST_TRANSACTIONS, () => ({
  status: 'pending'
}));

const fail = createAction(LAST_TRANSACTIONS, error => ({
  error,
  status: 'error'
}));

const success = createAction(LAST_TRANSACTIONS, transactions => ({
  transactions,
  status: 'success'
}));

const getLastTransactions = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: LAST_TRANSACTIONS_PATH
  })
    .then(response => {
      const transactions = response;
      dispatch(success(transactions.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getLastTransactions
};

export default actions;
