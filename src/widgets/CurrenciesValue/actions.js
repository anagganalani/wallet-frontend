import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { CURRENCIES_VALUE } from './constants';

const CURRENCIES_VALUE_PATH = process.env.REACT_APP_CURRENCIES_VALUE_PATH;

const reset = createAction(CURRENCIES_VALUE, () => ({
  status: 'initial'
}));

const begin = createAction(CURRENCIES_VALUE, () => ({
  status: 'pending'
}));

const fail = createAction(CURRENCIES_VALUE, error => ({
  error,
  status: 'error'
}));

const success = createAction(CURRENCIES_VALUE, values => ({
  values,
  status: 'success'
}));

const getValues = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: CURRENCIES_VALUE_PATH
  })
    .then(response => {
      const values = response;
      dispatch(success(values.data));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getValues
};

export default actions;
