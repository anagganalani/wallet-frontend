import React, { Component } from 'react';
import { connect } from 'react-redux';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import CardBody from 'components/Card/CardBody';
import actions from './actions';

class CurrenciesValue extends Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  // TODO: fix me
  UNSAFE_componentWillMount() {
    this.props.getValues();
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'success') {
      this.setState({ loading: false });
    }
  }

  render() {
    const { values = {} } = this.props;
    if (this.state.loading && Object.keys(values).length === 0)
      return 'Loading';
    const values_list = Object.keys(values).map(key => {
      return (
        <GridItem key={key}>
          <CardBody>
            {key}
            <br />
            {values[key]}
          </CardBody>
        </GridItem>
      );
    });

    return (
      <div>
        <Card>
          <CardHeader>Currencies value</CardHeader>
          <GridContainer>{values_list}</GridContainer>
        </Card>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  values: state.currencies_value.values,
  status: state.currencies_value.status
});
export default connect(mapStateToProps, actions)(CurrenciesValue);
