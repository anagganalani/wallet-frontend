import React, { Component } from 'react';
import { connect } from 'react-redux';
import CardHeader from 'components/Card/CardHeader';
import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';
import { Line } from 'react-chartjs-2';
import actions from './actions';

const MONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

class MonthEarnings extends Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  // TODO: fix me
  UNSAFE_componentWillMount() {
    this.props.getMonthEarnings();
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'success') {
      this.setState({ loading: false });
    }
  }

  render() {
    let { monthEarnings = [] } = this.props;
    const { loading } = this.state;
    if (loading && monthEarnings.length === 0) {
      return 'loading';
    }
    monthEarnings = Object.values(monthEarnings)
      .slice(0, 12)
      .map(earning => earning / 10 ** monthEarnings.scale)
      .reverse();
    const m = new Date().getMonth();
    const labels = MONTHS.slice(m + 1).concat(MONTHS.slice(0, m + 1));
    const data = {
      labels,
      datasets: [
        {
          label: 'Earning',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: monthEarnings
        }
      ]
    };
    return (
      <Card>
        <CardHeader>Month Earnings</CardHeader>
        <CardBody>
          <Line height={40} data={data} />
        </CardBody>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  monthEarnings: state.month_earnings.monthEarnings,
  status: state.month_earnings.status
});

export default connect(mapStateToProps, actions)(MonthEarnings);
