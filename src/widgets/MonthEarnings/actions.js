import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { MONTH_EARNINGS } from './constants';

const MONTH_EARNINGS_PATH = process.env.REACT_APP_MONTH_EARNINGS_PATH;

const reset = createAction(MONTH_EARNINGS, () => ({
  status: 'initial'
}));

const begin = createAction(MONTH_EARNINGS, () => ({
  status: 'pending'
}));

const fail = createAction(MONTH_EARNINGS, error => ({
  error,
  status: 'error'
}));

const success = createAction(MONTH_EARNINGS, monthEarnings => ({
  monthEarnings,
  status: 'success'
}));

const getMonthEarnings = () => dispatch => {
  dispatch(begin);
  fetchApi({
    path: MONTH_EARNINGS_PATH
  })
    .then(response => {
      const monthEarnings = response.data;
      dispatch(success(monthEarnings));
    })
    // TODO: handle error
    .catch(error => {
      throw error;
    });
};

const actions = {
  reset,
  begin,
  fail,
  success,
  getMonthEarnings
};

export default actions;
