import React, { Component } from 'react';
import { connect } from 'react-redux';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import CardBody from 'components/Card/CardBody';

class WalletBalance extends Component {
  constructor() {
    super();
    this.state = {
      // TODO: fix me
      // eslint-disable-next-line react/no-unused-state
      loading: true
    };
  }

  render() {
    const { balances } = this.props;
    let balances_list = '';
    if (balances.length > 0) {
      // eslint-disable-next-line
      balances_list = balances.map(balance => {
        const { id, currency, available, scale, status } = balance;
        if (status === 'enabled') {
          return (
            <GridItem key={id}>
              <CardBody>
                {currency}
                <br />
                {available / (10 ** scale).toFixed(3)}
              </CardBody>
            </GridItem>
          );
        }
      });
    }
    let total_balance = '';
    if (balances.length > 0) {
      const { id, currency, available, scale } = balances[balances.length - 1];
      total_balance = (
        <GridItem key={id}>
          <CardBody>
            Total balance {currency}
            <br />
            {available / (10 ** scale).toFixed(3)}
          </CardBody>
        </GridItem>
      );
    }

    return (
      <div>
        <Card>
          <CardHeader>Wallet</CardHeader>
          <GridContainer>
            {total_balance}
            {balances_list}
          </GridContainer>
        </Card>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  balances: state.wallet.data.length > 0 ? state.wallet.data : []
});
export default connect(mapStateToProps, {})(WalletBalance);
