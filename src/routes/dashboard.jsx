import Dashboard from 'pages/dashboard';
import Wallet from 'pages/wallet';

// @material-ui/icons
import DashboardIcon from '@material-ui/icons/Dashboard';
import WalletIcon from '@material-ui/icons/AccountBalanceWallet';

const dashboardRoutes = [
  {
    id: 0,
    path: '/dashboard',
    name: 'Dashboard',
    icon: DashboardIcon,
    component: Dashboard
  },
  {
    id: 1,
    path: '/wallet',
    name: 'Wallet',
    icon: WalletIcon,
    component: Wallet
  },
  {
    id: 2,
    redirect: true,
    path: '/',
    pathTo: '/dashboard',
    name: 'Dashboard'
  }
];
export default dashboardRoutes;
